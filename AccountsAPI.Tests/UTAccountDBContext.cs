﻿using AccountsAPI.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsAPI.Test
{
   public  class UTAccountDBContext
    {
        List<AccountUser> lstMockAccountUser = null;
        List<Roles> lstMockRoles = null;
        List<UserRoleMapping> lstMockRoleMapping = null;
        List<Account> lstMockAccounts = null;
        List<AccountBalanceUploadInfo> lstMockAccountBalanceUploadInfo = null;
        List<UploadedFileInfo> lstMockUploadFileInfo = null;

        public Mock<AccountsDBContext> mockContext = null;
        public UTAccountDBContext()
        {
            InitializeMockData();
        }
        private void InitializaAccountUser()
        {
            lstMockAccountUser = new List<AccountUser>
            {
                new AccountUser { UserName = "Sundt",Password="Sundt",UserID=1 },
                new AccountUser { UserName = "sajee",Password="sajee",UserID=2 },
                new AccountUser { UserName = "AAA" },
            };
        }

        private void InitializeRoles()
        {
            lstMockRoles = new List<Roles>
            {
                new Roles() { RoleName = "Admin", CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now,RoleID=1 },
               new Roles() { RoleName = "Regular", CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now,RoleID=2 }
            };
        }

        private void InitializeRoleMapping()
        {
            lstMockRoleMapping = new List<UserRoleMapping>
            {
                new UserRoleMapping() { UserID =1,RoleID=1 },
               new UserRoleMapping() {UserID =2,RoleID=2 }
            };
        }

        private void InitializeAccounts()
        {
            lstMockAccounts = new List<Account>
            {
                new Account() { AccountName= "R&D",Balance=100, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now ,AccountID=1},
               new Account() { AccountName = "Canteen", Balance = 0, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now,AccountID=2 }
            };
        }

        private void InitializeAccountBalanceInfo()
        {
            lstMockAccountBalanceUploadInfo = new List<AccountBalanceUploadInfo>
            {
                new AccountBalanceUploadInfo() {AccountID=1,Amount=10.0,CreatedDate=DateTime.Now },
              new AccountBalanceUploadInfo() {AccountID=1,Amount=10.0,CreatedDate=DateTime.Now.AddDays(-10) },
              new AccountBalanceUploadInfo() {AccountID=1,Amount=20.0,CreatedDate=DateTime.Now.AddMonths(-1) },
              new AccountBalanceUploadInfo() {AccountID=2,Amount=-50,CreatedDate=DateTime.Now.AddDays(0) },
              new AccountBalanceUploadInfo() {AccountID=2,Amount=10.0,CreatedDate=DateTime.Now.AddMonths(-1) },
            };
        }

        private void InitializeUploadFileInfo()
        {
            lstMockUploadFileInfo = new List<UploadedFileInfo>();
        }
        private void InitializeMockData()
        {
            
            InitializaAccountUser();
            InitializeRoles();
            InitializeRoleMapping();
            InitializeAccounts();
            InitializeAccountBalanceInfo();
            InitializeUploadFileInfo();
            
            mockContext = new Mock<AccountsDBContext>();
            mockContext.Setup(c => c.AccountsUser).Returns(GetQueryableMockDbSet<AccountUser>(lstMockAccountUser));
            mockContext.Setup(c => c.Roles).Returns(GetQueryableMockDbSet<Roles>(lstMockRoles));
            mockContext.Setup(c => c.UserRoleMapping).Returns(GetQueryableMockDbSet<UserRoleMapping>(lstMockRoleMapping));
            mockContext.Setup(c => c.Accounts).Returns(GetQueryableMockDbSet<Account>(lstMockAccounts));
            mockContext.Setup(c => c.AccountBalanceUploadInfo).Returns(GetQueryableMockDbSet<AccountBalanceUploadInfo>(lstMockAccountBalanceUploadInfo));
            mockContext.Setup(c => c.UploadedFileInfo).Returns(GetQueryableMockDbSet<UploadedFileInfo>(lstMockUploadFileInfo));
        }

        private static DbSet<T> GetQueryableMockDbSet<T>(List<T> sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            dbSet.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => sourceList.Add(s));

            return dbSet.Object;
        }

    }
}
