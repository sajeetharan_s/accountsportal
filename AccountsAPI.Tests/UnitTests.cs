﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountsAPI.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Moq;
using AccountsAPI.Services;
using System.Globalization;
using AccountsAPI.Models;
using System.IO;
using AccountsAPI.Interfaces;

namespace AccountsAPI.Test
{
    [TestClass]
    public class UnitTest1
    {
        IAccountRepository dbRepository = null;

        public UnitTest1()
        {
            dbRepository = new UTAccountRepository();
        }

        [TestMethod]
        public void ValidateUserSuccess()
        {
            UserService userService = new UserService(dbRepository);
            var response = userService.ValidateUser("santhu", "santhu");
            Assert.AreEqual("Admin", response.Role);
        }

        [TestMethod]
        public void ValidateUserFailure()
        {
            UserService userService = new UserService(dbRepository);
            var response = userService.ValidateUser("sajee", "password");
            Assert.AreEqual(null, response);
        }

        [TestMethod]
        public void GetAccountBalance()
        {
            AccountService accountService = new AccountService(dbRepository);
            var response = accountService.GetAccountBalances();
            Assert.AreEqual("100", response[0].Amount);
        }

        [TestMethod]
        public void GetAccountBalanceForChartTestMonth()
        {
            DateTime startDate = DateTime.Now.AddMonths(-4);
            DateTime endDate = DateTime.Now;
            AccountService accountService = new AccountService(dbRepository);
            var response = accountService.GetAccountBalanceForChart(startDate, endDate);
            Assert.AreEqual(5, response.lstMonths.Count);
        }

        [TestMethod]
        public void GetAccountBalanceForChartTestBalance()
        {
            DateTime startDate = DateTime.Now.AddMonths(-4);
            DateTime endDate = DateTime.Now;
            string month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
            AccountService accountService = new AccountService(dbRepository);
            var response = accountService.GetAccountBalanceForChart(startDate, endDate);
            var data = response.lstBalances.Where(x => x.AccountName == "R&D").FirstOrDefault().AccountBalanceOnMonth.Where(y => y.MonthName == month).FirstOrDefault();
            Assert.AreEqual("10", data.Amount);
        }

        [TestMethod]
        public void ValidateAccountBalanceTestSuccess()
        {
            List<AccountBalanceFileInfo> lstBalance = new List<AccountBalanceFileInfo>();
            lstBalance.Add(new AccountBalanceFileInfo() { AccountName = "Canteen", AccountID = 1, Amount = "10" });
            UploadService uploadService = new UploadService(dbRepository);
            PrivateObject obj = new PrivateObject(uploadService);
            var retVal = (List<AccountBalanceFileInfo>)obj.Invoke("ValidateAccountBalanace", lstBalance);
            Assert.AreEqual(2, retVal[0].AccountID);
        }

        [TestMethod]
        public void ValidateAccountBalanceTestInvalidAccount()
        {
            List<AccountBalanceFileInfo> lstBalance = new List<AccountBalanceFileInfo>();
            lstBalance.Add(new AccountBalanceFileInfo() { AccountName = "Canteen123", AccountID = 1, Amount = "10" });
            UploadService uploadService = new UploadService(dbRepository);
            PrivateObject obj = new PrivateObject(uploadService);
            var retVal = (List<AccountBalanceFileInfo>)obj.Invoke("ValidateAccountBalanace", lstBalance);
            Assert.AreEqual(false, retVal[0].isValid);
        }

        [TestMethod]
        public void UploadAccountInfoTestSuccess()
        {
            UploadService uploadService = new UploadService(dbRepository);
            StreamReader reader = File.OpenText("sample.txt");
            var response=uploadService.UploadAccountInfo("sample.txt", reader.BaseStream);
            Assert.AreEqual("File uploaded successfully", response.Message);
        }

        [TestMethod]
        public void UploadAccountInfoTestBalance()
        {
            UploadService uploadService = new UploadService(dbRepository);
            StreamReader reader = File.OpenText("sample.txt");
            var response = uploadService.UploadAccountInfo("sample.txt", reader.BaseStream);
            var account = dbRepository.GetAllAccounts().Where(x => x.AccountName == "Canteen").FirstOrDefault();
            Assert.AreEqual(account.Balance, 50000);
        }
    }
}
