# Account Portal App
Web enabled  account balance overview tool.

# Requirements
  - Visual Studio 2017/2015
  - Azure Portal(SQL Azure,Web app)
  - VScode
  - Node

### Installation
##### WEB API
- Clone the repository https://bitbucket.org/sajeetharan_s/accountsportal
- Open the solution AccountsApp using Visual Studio as Admininstrator
- Restore the dependencies by right click on the solution and restore nuget packages
- Change the connection string with sql azure credentials in web.config
```
 <add name="AccountsDBContext" connectionString="Data Source=accountportal.database.windows.net;Initial Catalog=AccountPortal;Persist Security Info=True;User ID=sajeetharan;Password=BalanceViewer2017" providerName="System.Data.SqlClient" />
```
 - Make sure your IP is configured under firewall rules on sql server in azure portal 
 ![follow this image](https://image.ibb.co/fkgLbG/document.jpg)
 -  Build the solution
 -  Run the project AccountsAPI
 -  API should be running on http://localhost:63764/
 -  To run unit tests on web api navigate to Test->Run-> All tests
 -  Once build is success , test for api is configured as post-build event by adding the following on the AccountAPI.Test project

```
CD $(TargetDir)"$(DevEnvDir)MSTEST.exe" /testcontainer:AccountsAPI.Test.dll
```
![follow this](https://image.ibb.co/gncigG/postbuild.jpg)

##### Client App
 - Navigate to AccountsPortalApp
```sh
 C:\accountsportal\AccountsPortalApp>
```
 - Run npm install -g @angular/cli@latest
```sh
 C:\accountsportal\AccountsPortalApp>npm install -g @angular/cli@latest
```
 - Run npm install
```sh
 C:\accountsportal\AccountsPortalApp>npm install
```
 - Add the web api end point in AccountsPortalApp/src/environments/environment.ts
```
export const environment = {
  production: false,
  apiUrl: 'http://localhost:63764/'
};

```
 - Run ng serve for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files
```sh
 C:\accountsportal\AccountsPortalApp>ng serve
```
### Build

 - Run ng build to build the project. The build artifacts will be stored in the dist/ directory. Use the -prod flag for a production build.
```sh
 C:\accountsportal\AccountsPortalApp>ng build
```
### Unit tests
 - Run ng test to execute the unit tests via Karma.
```sh
 C:\accountsportal\AccountsPortalApp>ng test
```
  - Test coverage results
Run ng test --single-run --code-coverage and see the generated coverage results under AccountsPortalApp /coverage/index.html
```sh
 C:\accountsportal\AccountsPortalApp>ng test --single-run --code-coverage
```

## Unit tests and build together
 - Run npm run-script testnbuild to execute the unit tests via Karma.
```sh
 C:\accountsportal\AccountsPortalApp>npm run-script testnbuild
```

### e2e tests
 - Run ng e2e  to execute the end to end testing using protractor
```sh
 C:\accountsportal\AccountsPortalApp>ng e2e
```

## Usage
There are bbasically two types of users in the system, 
#### admin user
  - Login to the system with the credentials 
  username:Sundt
  password:Sundt

#### Regular user
  - Login to the system as regular user with the credentials
  username:John
  password:John
  - Rest of the users can be added on the database in the table AccountUser
  - Inially Accounts are created for all the 5 accounts
    R&D
    Canteen
    CEO's car
    Marketing
    Parking fines
  - More accounts can be added on the database in the table Account
  - If it’s a new server invoke the service using postman as follows to create the default tables in new database
   ![follow this](https://image.ibb.co/esnJwG/token.jpg)

## Features
#### Login
![Login](https://image.ibb.co/mfyVAb/login.gif)

##### Balance viewer(Regular User)
![Regular user view ](https://image.ibb.co/jzpWGG/regular_user.gif)

##### File upload(Admin User)
![Admin file upload ](https://image.ibb.co/h1n7qb/admin_file_txt_excel.gif)

##### File upload(Admin User)
![Admin file upload ](https://image.ibb.co/h1n7qb/admin_file_txt_excel.gif)

##### Report view(Admin User)
![Admin report  ](https://preview.ibb.co/nQ6vbG/admin_report_oneview.gif)

### Sample files to upload 
[file1](https://uploadfiles.io/wiqpb)
[file2](https://ufile.io/chm29)
### Todos
 - Write MORE Tests

License
----

MIT

**Free Software, Hell Yeah!**
 
