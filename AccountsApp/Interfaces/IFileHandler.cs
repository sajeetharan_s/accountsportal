﻿using AccountsAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AccountsAPI.Interfaces
{
    public interface IFileHandler
    {
        List<AccountBalanceFileInfo> ParseFile(Stream fileData);
    }
}