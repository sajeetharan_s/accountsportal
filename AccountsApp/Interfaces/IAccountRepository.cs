﻿using AccountsAPI.Models;
using AccountsAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsAPI.Interfaces
{
    public interface IAccountRepository
    {
        List<AccountInfo> GetAllAccounts();
        List<AccountBalPerMonth> GetAccountBalanceForChart(DateTime startDate,DateTime endDate);
        int InsertFileInfo(string fileName, string userName);
        bool InsertAccountBalanceInfo(List<AccountAmountInfo> lstAccountAmountInfo);
        User ValidateAndGetUser(string username, string password);
    }
}
