﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Models
{
    public class AccountBalanceFileInfo
    {
        [JsonIgnore]
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public string Amount { get; set; }
        [JsonIgnore]
        public bool isValid { get; set; }
        [JsonIgnore]
        public double ConvertedAmount { get; set; }
    }
}