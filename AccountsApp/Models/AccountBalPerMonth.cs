﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Models
{
    public class AccountBalPerMonth
    {
        public string AccountName { get; set; }
        public DateTime CreatedDate { get; set; }
        public double Amount { get; set; }
    }
}