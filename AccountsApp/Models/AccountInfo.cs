﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Models
{
    public class AccountInfo
    {
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public double Balance { get; set; }
    }
}