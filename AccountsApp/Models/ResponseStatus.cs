﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Models
{
    public class ResponseStatus
    {
        public bool hasError { get; set; }
        public string Message { get; set; }
    }
}