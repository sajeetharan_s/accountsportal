﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Models
{
    public class AccountAmountInfo
    {
        public int AccountID { get; set; }
        public int FileID { get; set; }
        public string UserName { get; set; }
        public double Amount { get; set; }
    }
}