﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Models
{
    public class AccountBalChartModel
    {
        public string AccountName { get; set; }
        public List<AccountBalForMonth> AccountBalanceOnMonth { get; set; }
    }

    public class AccountBalForMonth
    {
        public string MonthName { get; set; }
        public string Amount { get; set; }
    }

    public class BalMonthWiseModel
    {
        public List<string> lstMonths { get; set; }
        public List<AccountBalChartModel> lstBalances { get; set; }
    }
}