﻿using AccountsAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AccountsAPI.Models;
using AccountsAPI.Repository;

namespace AccountsAPI.DAL
{
    public class AccountRepository : IAccountRepository
    {
        private AccountsDBContext dBContext;
        public AccountRepository(AccountsDBContext _accountsDBContext)
        {
            dBContext = _accountsDBContext;
        }
        public List<AccountBalPerMonth> GetAccountBalanceForChart(DateTime startDate, DateTime endDate)
        {
            var accountBalances = dBContext.AccountBalanceUploadInfo
                                    .Join(dBContext.Accounts, x => x.AccountID, y => y.AccountID,
                                            (x, y) => new { Balance = x, Account = y })
                                    .Select(x => new AccountBalPerMonth {
                                                   AccountName= x.Account.AccountName,
                                                   Amount=x.Balance.Amount,
                                                    CreatedDate=x.Balance.CreatedDate })
                                    .Where(x => x.CreatedDate >= startDate && x.CreatedDate <= endDate).ToList();
            return accountBalances;
        }

        public List<AccountInfo> GetAllAccounts()
        {
            List<AccountInfo> accounts = dBContext.Accounts.Select(x => new AccountInfo
            {
                AccountID = x.AccountID,
                AccountName = x.AccountName,
                Balance = x.Balance
            }).ToList();
           return accounts;
        }

        public bool InsertAccountBalanceInfo(List<AccountAmountInfo> lstAccountAmountInfo)
        {
            bool isSuccess = false;
            AccountBalanceUploadInfo accountBalanceUploadInfo = null;
            foreach (var accounts in lstAccountAmountInfo)
            {

                accountBalanceUploadInfo = new AccountBalanceUploadInfo();
                accountBalanceUploadInfo.FileID = accounts.FileID;
                accountBalanceUploadInfo.AccountID = accounts.AccountID;
                accountBalanceUploadInfo.Amount = accounts.Amount;

                accountBalanceUploadInfo.CreatedBy = accounts.UserName;
                accountBalanceUploadInfo.CreatedDate = DateTime.Now;
                accountBalanceUploadInfo.LastModifiedBy = accounts.UserName;
                accountBalanceUploadInfo.LastModifiedDate = DateTime.Now;
                dBContext.AccountBalanceUploadInfo.Add(accountBalanceUploadInfo);
                var account = dBContext.Accounts.Where(x => x.AccountID == accounts.AccountID).FirstOrDefault();
                if (account != null)
                {
                    account.Balance = account.Balance + accounts.Amount;
                    account.LastModifiedBy = accounts.UserName;
                    account.LastModifiedDate = DateTime.Now;
                }
            }
            dBContext.SaveChanges();
            return isSuccess;
        }

        public int InsertFileInfo(string fileName, string userName)
        {
            int id = 0;
            UploadedFileInfo fileInfo = new UploadedFileInfo();
            fileInfo.FileName = fileName;
            fileInfo.CreatedBy = userName;
            fileInfo.CreatedDate = DateTime.Now;
            fileInfo.LastModifiedBy = userName;
            fileInfo.LastModifiedDate = DateTime.Now;
            dBContext.UploadedFileInfo.Add(fileInfo);
            dBContext.SaveChanges();
            id = fileInfo.FileID;
            return id;
        }

        public User ValidateAndGetUser(string username, string password)
        {
            User user = null;

            var accountUser = dBContext.AccountsUser.Where(x => x.UserName.ToLower() == username.ToLower() && x.Password == password)
                              .FirstOrDefault();

            if (accountUser != null)
            {
                user = new User();
                user.Id = accountUser.UserID;
                user.Name = accountUser.UserName;
                var role = dBContext.UserRoleMapping.Join(dBContext.Roles, rolemapping => rolemapping.RoleID,
                                roles => roles.RoleID,
                                (rolemapping, roles) => new { rolemapping, roles }
                            ).Where(x => x.rolemapping.UserID == user.Id).FirstOrDefault();
                if (role != null)
                {
                    user.Role = role.roles.RoleName;
                }
                else
                {
                    user.Role = "";
                }
            }
            return user;
        }
    }
}