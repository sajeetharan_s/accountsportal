﻿using AccountsAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AccountsAPI.Models;
using System.IO;

namespace AccountsAPI.Services
{
    public class TextFileProcessor : IFileHandler
    {
        public List<AccountBalanceFileInfo> ParseFile(Stream fileData)
        {
            List<AccountBalanceFileInfo> lstAccountBalances = new List<AccountBalanceFileInfo>();
            string content = ReadFile(fileData);
            if (!string.IsNullOrEmpty(content))
            {
                AccountBalanceFileInfo accountBalance;
                string[] lines = content.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    string[] accountInfo = line.Split("\t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (accountInfo.Length == 2)
                    {
                        string accountName = accountInfo[0].Trim();
                        string balance = accountInfo[1].Trim();
                        accountBalance = new AccountBalanceFileInfo();
                        accountBalance.AccountName = accountName;
                        accountBalance.Amount= balance;
                        lstAccountBalances.Add(accountBalance);
                    }
                }
            }
            return lstAccountBalances;
        }

        private string ReadFile(Stream fileData)
        {
            System.IO.StreamReader myReader = new System.IO.StreamReader(fileData);
            string sFileData = myReader.ReadToEnd();
            
            return sFileData;
        }
    }
}