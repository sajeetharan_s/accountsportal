﻿using AccountsAPI.DAL;
using AccountsAPI.Interfaces;
using AccountsAPI.Models;
using AccountsAPI.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AccountsAPI.Services
{
    public class AccountService
    {
        List<AccountBalanceFileInfo> lstAccountBalances = null;
        List<AccountBalChartModel> lstChartData = null;
        IAccountRepository accountRepository = null;
        public AccountService(IAccountRepository _accountRepository)
        {
            accountRepository = _accountRepository;
        }
        public AccountService()
        {
            accountRepository = new AccountRepository(new AccountsDBContext());
        }

        public List<AccountBalanceFileInfo> GetAccountBalances()
        {
            var accounts = accountRepository.GetAllAccounts();
            if (accounts != null && accounts.Count > 0)
            {
                lstAccountBalances = accounts.Select(x => new AccountBalanceFileInfo()
                {
                    AccountName = x.AccountName,
                    Amount = x.Balance.ToString()
                }).ToList();
            }
            return lstAccountBalances;
        }

        public BalMonthWiseModel GetAccountBalanceForChart(DateTime startDate, DateTime endDate)
        {
            int currentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;
            BalMonthWiseModel balMonthWiseModel = new BalMonthWiseModel();
            
            if (endDate > DateTime.Now)
                endDate = DateTime.Now;
            int startMonth = startDate.Month;
            int endMonth = endDate.Month;
            List<string> lstMonths = new List<string>();
            for (int i = startMonth; i <= endMonth; i++)
            {
                lstMonths.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i));
            }
            var accounts = accountRepository.GetAllAccounts();
            var accountBalances = accountRepository.GetAccountBalanceForChart(startDate,endDate);

            if (accountBalances != null)
            {
                var balancePerMonth =
                            from c in accountBalances
                            group c by new
                            {
                                c.AccountName,
                                c.CreatedDate.Month
                            } into gcs
                            select new
                            {
                                AccountName = gcs.Key.AccountName,
                                Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(gcs.Key.Month),
                                Amount = gcs.Sum(x => x.Amount)
                            };
                AccountBalChartModel accountBalChartModel = null;
                lstChartData = new List<AccountBalChartModel>();
                foreach (var item in accounts)
                {
                    accountBalChartModel = new AccountBalChartModel();
                    accountBalChartModel.AccountName = item.AccountName;
                    var accbalance = balancePerMonth.Where(x => x.AccountName == item.AccountName).ToList();
                    if (accbalance != null)
                    {
                        var data = (from mon in lstMonths
                                   join bal in accbalance on mon equals bal.Month into tmpBal
                                   from tmp in tmpBal.DefaultIfEmpty()
                                   select new AccountBalForMonth
                                   {
                                       MonthName=mon,Amount=(tmp!=null?tmp.Amount.ToString():"0")
                                   }).Distinct().ToList();
                        accountBalChartModel.AccountBalanceOnMonth = data;
                    }
                    lstChartData.Add(accountBalChartModel);
                }
               balMonthWiseModel.lstBalances = lstChartData;
            }
            
            balMonthWiseModel.lstMonths = lstMonths;
            return balMonthWiseModel;
        }
    }
}