﻿using AccountsAPI.Interfaces;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AccountsAPI.Models;

namespace AccountsAPI.Services
{
    public class ExcelFileProcessor: IFileHandler
    {
        public List<AccountBalanceFileInfo> ParseFile(Stream fileData)
        {
            return ReadFile(fileData);
        }

        private List<AccountBalanceFileInfo> ReadFile(Stream fileName)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                
                int rowCount = sheetData.Elements<Row>().Count();
                int rowIndex = 0;
                AccountBalanceFileInfo accountBalance;
                List<AccountBalanceFileInfo> lstAccountBalances = new List<AccountBalanceFileInfo>();
                foreach (Row r in sheetData.Elements<Row>())
                {
                    int cellCount = r.Elements<Cell>().Count();
                    if (rowIndex != 0)
                    {
                        if (cellCount == 2)
                        {

                           var acc= r.ElementAt(0);
                            string accountName = ReadExcelCell((Cell)acc, workbookPart);
                            var bal = r.ElementAt(1);
                            string balance = ReadExcelCell((Cell)bal, workbookPart);
                            accountBalance = new AccountBalanceFileInfo();
                            accountBalance.AccountName = accountName;
                            accountBalance.Amount = balance;
                            lstAccountBalances.Add(accountBalance);
                        }
                    }
                    rowIndex++;
                }
                return lstAccountBalances;
            }
        }

        private string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }
            return (text ?? string.Empty).Trim();
        }
    }
}