﻿using AccountsAPI.DAL;
using AccountsAPI.Interfaces;
using AccountsAPI.Models;
using AccountsAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsAPI.Services
{
    public class UserService
    {
        IAccountRepository accountRepository = null;
        public UserService(IAccountRepository _accountRepository)
        {
            accountRepository = _accountRepository;
        }
        public UserService()
        {
            accountRepository = new AccountRepository(new AccountsDBContext());
        }
        public User ValidateUser(string username, string password)
        {
            // Here you can write the code to validate
            // User from database and return accordingly
            // To test we use dummy list here

            User user = accountRepository.ValidateAndGetUser(username, password);
            return user;
        }
    }

}