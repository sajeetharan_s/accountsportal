﻿using AccountsAPI.DAL;
using AccountsAPI.Interfaces;
using AccountsAPI.Models;
using AccountsAPI.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AccountsAPI.Services
{
    public class UploadService
    {
        IAccountRepository accountRepository = null;
        string loggedUser = "";
        public UploadService(IAccountRepository _accountRepository)
        {
            accountRepository = _accountRepository;
        }
        public UploadService()
        {
            accountRepository = new AccountRepository(new AccountsDBContext());
        }
        public ResponseStatus UploadAccountInfo(string fileName, Stream fileData)
        {
            ResponseStatus result = new ResponseStatus();
            IFileHandler fileHandler = null;
            List<Models.AccountBalanceFileInfo> lstAccountBalance = null;
            if (fileName.Contains(".txt"))
                fileHandler = new TextFileProcessor();
            else if (fileName.Contains(".xlsx"))
                fileHandler = new ExcelFileProcessor();
            if (fileHandler != null)
            {
                lstAccountBalance = fileHandler.ParseFile(fileData);
                if (lstAccountBalance != null && lstAccountBalance.Count > 0)
                {
                    bool isValid = false;
                    lstAccountBalance = ValidateAccountBalanace(lstAccountBalance);
                    isValid = lstAccountBalance.All(x => x.isValid);
                    if (isValid)
                    {
                        int fileID = accountRepository.InsertFileInfo(fileName,loggedUser);
                        AccountAmountInfo accountBalanceUploadInfo = null;
                        List<AccountAmountInfo> lstAccountAmount = new List<AccountAmountInfo>();
                        foreach (var accounts in lstAccountBalance)
                        {
                            accountBalanceUploadInfo = new AccountAmountInfo();
                            accountBalanceUploadInfo.FileID = fileID;
                            accountBalanceUploadInfo.AccountID = accounts.AccountID;
                            accountBalanceUploadInfo.Amount = accounts.ConvertedAmount;
                            accountBalanceUploadInfo.UserName = loggedUser;
                            lstAccountAmount.Add(accountBalanceUploadInfo);
                        }
                        accountRepository.InsertAccountBalanceInfo(lstAccountAmount);
                        result.hasError = false;
                        result.Message = "File uploaded successfully";
                    }
                    else
                    {
                        if (lstAccountBalance.Any(x => x.AccountID == 0))
                        {
                            string accoountNames = string.Join(",", lstAccountBalance.Where(x => x.AccountID == 0).Select(x => x.AccountName).ToList());
                            result.hasError = true;
                            result.Message = "Invalid accounts " + accoountNames;
                        }
                    }
                }
                else
                {
                    result.hasError = true;
                    result.Message = "Invalid file format or not records available. Please upload txt files";
                }
            }
            else
            {
                result.hasError = true;
                result.Message = "Invalid file type. Please upload txt files";
            }
            return result;
        }

        private List<Models.AccountBalanceFileInfo> ValidateAccountBalanace(List<Models.AccountBalanceFileInfo> lstAccountBalance)
        {
            bool isValid = false;
            var accountTable = accountRepository.GetAllAccounts();
            foreach (var accounts in lstAccountBalance)
            {
                isValid = false;
                var account = accountTable.Where(x => x.AccountName.Trim() == accounts.AccountName.Trim()).FirstOrDefault();
                if (account != null)
                {
                    isValid = true;
                    double bal = 0.0;
                    isValid = Double.TryParse(accounts.Amount, out bal);
                    if (isValid)
                    {
                        accounts.AccountID = account.AccountID;
                        accounts.ConvertedAmount = bal;
                    }
                }
                accounts.isValid = isValid;
            }
            return lstAccountBalance;
        }
    }
}