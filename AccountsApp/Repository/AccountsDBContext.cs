﻿namespace AccountsAPI.Repository
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;
   

    public class AccountsDBContext : DbContext
    {
        // Your context has been configured to use a 'AccountsDBContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'AuthenticationApp.Repository.AccountsDBContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'AccountsDBContext' 
        // connection string in the application configuration file.
        public AccountsDBContext()
            : base()
        {
            //Database.SetInitializer<AccountsDBContext>(new CreateDatabaseIfNotExists<AccountsDBContext>());
            this.Database.CommandTimeout = 180;
            Database.SetInitializer(new AccountDBInitializer());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<AccountUser> AccountsUser { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<UserRoleMapping> UserRoleMapping { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountBalanceUploadInfo> AccountBalanceUploadInfo { get; set; }
        public virtual DbSet<UploadedFileInfo> UploadedFileInfo { get; set; }
    }
    public class AccountDBInitializer : CreateDatabaseIfNotExists<AccountsDBContext>
    {
        protected override void Seed(AccountsDBContext context)
        {
            AccountUser user = new AccountUser();
            user.UserName = "Sundt";
            user.DisplayName = "Sundt";
            user.Password = "Sundt";
            user.CreatedBy = "Portal";
            user.CreatedDate = DateTime.Now;
            user.LastModifiedBy = "Portal";
            user.LastModifiedDate = DateTime.Now;
            context.AccountsUser.Add(user);
            context.SaveChanges();

            AccountUser user1 = new AccountUser();
            user1.UserName = "John";
            user1.DisplayName = "John";
            user1.Password = "John";
            user1.CreatedBy = "Portal";
            user1.CreatedDate = DateTime.Now;
            user1.LastModifiedBy = "Portal";
            user1.LastModifiedDate = DateTime.Now;
            context.AccountsUser.Add(user1);
            context.SaveChanges();

            Roles adminRole = new Roles() { RoleName = "Admin", CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            Roles regularRole = new Roles() { RoleName = "Regular", CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            context.Roles.Add(adminRole);
            context.Roles.Add(regularRole);
            context.SaveChanges();

            UserRoleMapping adminRoleMapping = new UserRoleMapping();
            adminRoleMapping.UserID = context.AccountsUser.Where(x => x.UserName == "Sundt").FirstOrDefault().UserID;
            adminRoleMapping.RoleID = context.Roles.Where(x => x.RoleName == "Admin").FirstOrDefault().RoleID;
            adminRoleMapping.CreatedBy = "Portal";
            adminRoleMapping.CreatedDate = DateTime.Now;
            adminRoleMapping.LastModifiedBy = "Portal";
            adminRoleMapping.LastModifiedDate = DateTime.Now;
            context.UserRoleMapping.Add(adminRoleMapping);
            context.SaveChanges();


            UserRoleMapping regularRoleMapping = new UserRoleMapping();
            regularRoleMapping.UserID = context.AccountsUser.Where(x => x.UserName == "John").FirstOrDefault().UserID;
            regularRoleMapping.RoleID = context.Roles.Where(x => x.RoleName == "Regular").FirstOrDefault().RoleID;
            regularRoleMapping.CreatedBy = "Portal";
            regularRoleMapping.CreatedDate = DateTime.Now;
            regularRoleMapping.LastModifiedBy = "Portal";
            regularRoleMapping.LastModifiedDate = DateTime.Now;
            context.UserRoleMapping.Add(regularRoleMapping);
            context.SaveChanges();

            Account rndAccount=new Account() { AccountName= "R&D",Balance=0, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            Account canteenAccount = new Account() { AccountName = "Canteen", Balance = 0, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            Account ceoAccount = new Account() { AccountName = "CEO's car", Balance = 0, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            Account marktAccount = new Account() { AccountName = "Marketing", Balance = 0, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            Account parkAccount = new Account() { AccountName = "Parking fines", Balance = 0, CreatedBy = "Portal", CreatedDate = DateTime.Now, LastModifiedBy = "Portal", LastModifiedDate = DateTime.Now };
            context.Accounts.Add(rndAccount);
            context.Accounts.Add(canteenAccount);
            context.Accounts.Add(ceoAccount);
            context.Accounts.Add(marktAccount);
            context.Accounts.Add(parkAccount);
            context.SaveChanges();
            base.Seed(context);
        }
    }

    public class AccountUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }

    public class Roles
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }

    public class UserRoleMapping
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UserRoleID { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}